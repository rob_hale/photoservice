﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using PhotoRepository_Web_App.Classes;
using PhotoRepository_Web_App.Interfaces;
using PhotoSettings;

namespace ConsoleApplication1
{

    public class Program
    {
        private static string _servicesBusConnectionString = string.Empty;
        private static string _storageAccountConnectionString = string.Empty;
        private static string _dashBoardConnectionString = string.Empty;
        private static IPhotoBlobManager _photoBlobUploader = null;
        public static void Main(string[] args)
        {
            Init();
            var config = new JobHostConfiguration
            {
                ServiceBusConnectionString = _servicesBusConnectionString,
                StorageConnectionString = _storageAccountConnectionString,
                DashboardConnectionString = _dashBoardConnectionString
            };
            var host = new JobHost(config);
            host.RunAndBlock();
        }

        public static void Init()
        {
            _servicesBusConnectionString = ConfigurationManager.ConnectionStrings[Storage.ServiceBusConnection].ConnectionString;
            _storageAccountConnectionString =
                ConfigurationManager.ConnectionStrings[Storage.StorageAccountName].ConnectionString;
            _dashBoardConnectionString =
                ConfigurationManager.ConnectionStrings[Storage.DashboardConnection].ConnectionString;
            _photoBlobUploader = new PhotoPhotoBlobManager();

        }
        public async Task SBQListener([ServiceBusTrigger("sbimagequeue")] BrokeredMessage message)
        {
            Console.WriteLine("Intercepted brokered message with " + message.Properties.Count.ToString(CultureInfo.InvariantCulture) + " properties");
            String fileName = message.Properties["FileName"].ToString();
            Console.WriteLine("Processing image " + fileName);
            byte[] photoBytes = await _photoBlobUploader.GetPhotoFromBlob(fileName);

            MemoryStream inStream = new MemoryStream(photoBytes);
            string[] photoNames = await _photoBlobUploader.ResizePhotos(inStream, fileName);
        }
    }
}
