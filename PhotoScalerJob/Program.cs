﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using PhotoRepository_Web_App.Classes;
using PhotoRepository_Web_App.Interfaces;
using PhotoSettings;

namespace ConsoleApplication1
{
    /// <summary>
    /// Can be used to resolve % % tokens from the config file
    /// </summary>
    internal class ConfigNameResolver : INameResolver
    {
        public string Resolve(string name)
        {
            string resolvedName = ConfigurationManager.AppSettings[name];
            if (string.IsNullOrWhiteSpace(resolvedName))
            {
                throw new InvalidOperationException("Cannot resolve " + name);
            }

            return resolvedName;
        }
    }
    public class Program
    {

        private static string _servicesBusConnectionString = string.Empty;
        private static string _storageAccountConnectionString = string.Empty;
        private static string _dashBoardConnectionString = string.Empty;
        private static IPhotoBlobManager _photoBlobUploader = null;

        public static void Main(string[] args)
        {
            Init();
            var config = new JobHostConfiguration
            {
                ServiceBusConnectionString = _servicesBusConnectionString,
                StorageConnectionString = _storageAccountConnectionString,
                DashboardConnectionString = _dashBoardConnectionString,
                NameResolver = new ConfigNameResolver()
            };
            var host = new JobHost(config);
            host.RunAndBlock();
        }

        public static void Init()
        {
            _servicesBusConnectionString =      CloudConfigurationManager.GetSetting(Storage.ServiceBusConnection);
            _storageAccountConnectionString =   CloudConfigurationManager.GetSetting(Storage.StorageAccountName);
            _dashBoardConnectionString =        CloudConfigurationManager.GetSetting(Storage.DashboardConnection);
            _photoBlobUploader = new PhotoPhotoBlobManager();

        }
        public async Task SBQListener([ServiceBusTrigger("%ServiceBusQueueName%")] BrokeredMessage message)
        {
            Console.WriteLine("Intercepted brokered message with " + message.Properties.Count.ToString(CultureInfo.InvariantCulture) + " properties");
            String fileName = message.Properties["FileName"].ToString();
            Console.WriteLine("Processing image " + fileName);
            byte[] photoBytes = await _photoBlobUploader.GetPhotoFromBlob(fileName);

            MemoryStream inStream = new MemoryStream(photoBytes);
            string[] photoNames = await _photoBlobUploader.ResizePhotos(inStream, fileName);
        }
    }
}
