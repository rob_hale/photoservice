﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoSettings
{
    public static class Storage
    {
        public readonly static string BlobName = "images";
        public readonly static string StorageAccountName = "StorageAccount";
        public readonly static string ServiceBusConnection = "ServiceBusConnection";
        public readonly static string DashboardConnection = "DashBoardConnection";
        public readonly static string HostUri = "HostUri";
    }
}
