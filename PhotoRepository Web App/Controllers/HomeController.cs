﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using PhotoRepository_Web_App.Classes;
using PhotoRepository_Web_App.Interfaces;

namespace PhotoRepository_Web_App.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPhotoBlobManager _photoBlobUploader;
        private readonly ISbQueueManager _sbQueueManager;
        public HomeController(): this(new PhotoPhotoBlobManager(), new SbQueueManager())
        {
            
        }

        public HomeController(IPhotoBlobManager bUploader, ISbQueueManager queueManager)
        {
            _photoBlobUploader = bUploader;
            _sbQueueManager = queueManager;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {
            if (file == null)
            {
                return View(0);
            }

            _photoBlobUploader.FileName = Path.GetFileName(file.FileName);
            await _photoBlobUploader.UploadFromHttpPostedFile(file);
            await _sbQueueManager.SendImageName(_photoBlobUploader.FileName);
            ViewBag.Results = "Successfully uploaded " + _photoBlobUploader.FileName + "  " + file.ContentLength + " bytes";
            return View(file.ContentLength);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadFiles(HttpPostedFileBase[] files)
        {
            foreach (var postedFile in files)
            {
                _photoBlobUploader.FileName = Path.GetFileName(postedFile.FileName);
                await _photoBlobUploader.UploadFromHttpPostedFile(postedFile);
                await _sbQueueManager.SendImageName(_photoBlobUploader.FileName);
                ViewBag.Results += "Successfully uploaded " + _photoBlobUploader.FileName + "  " + postedFile.ContentLength + " bytes<br/>";
            }
            return View();
        }

        public ActionResult UploadFiles()
        {
            return View();
        }

    }
}