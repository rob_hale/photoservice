﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using PhotoRepository_Web_App.Interfaces;
using PhotoSettings;

namespace PhotoRepository_Web_App.Class_Implementation
{
    public class BlobConnection : IBlobConnection
    {
        public BlobConnection() : this(CloudConfigurationManager.GetSetting(Storage.StorageAccountName))
        {

        }

        public BlobConnection(String connectionString)
        {

            ConnectionString = connectionString;

            try
            {
                CloudStorageAccount = CloudStorageAccount.Parse(ConnectionString);
                CloudBlobClient = CloudStorageAccount.CreateCloudBlobClient();
            }
            catch (Exception e)
            {
                ArgumentException ae = new ArgumentException("Could not create blob connection with default values", e);
                throw ae;
            }
        }

        public string ConnectionString { get; set; }

        public CloudStorageAccount CloudStorageAccount { get; set; }

        public CloudBlobClient CloudBlobClient { get; set; }
        public CloudBlobContainer BlobContainer { get; set; }

        public async Task<CloudBlobContainer> GetBlobContainer(String blobName)
        {
            BlobContainer = null;
            try
            {
                BlobContainer = CloudBlobClient.GetContainerReference(Storage.BlobName);
                await BlobContainer.CreateIfNotExistsAsync();
            }
            catch (Exception e)
            {
                BlobContainer = null;
                throw;
            }
            return BlobContainer;
        }

        public async Task<CloudBlockBlob> GetBlob(string blobName)
        {
            if (BlobContainer == null)
            {
                try
                {
                    BlobContainer = CloudBlobClient.GetContainerReference(Storage.BlobName);
                    await BlobContainer.CreateIfNotExistsAsync();
                }
                catch (Exception e)
                {
                    BlobContainer = null;
                    throw;
                }
            }

            CloudBlockBlob blockBlob = BlobContainer.GetBlockBlobReference(blobName);
            return blockBlob;
        }

        public async Task<long> UploadFromMemoryStream(MemoryStream mStream, string blobName)
        {
            CloudBlockBlob blob = BlobContainer.GetBlockBlobReference(blobName);
            blob.StreamWriteSizeInBytes = 256*1024; //256 k
            mStream.Position = 0;
            await blob.UploadFromStreamAsync(mStream);
            await mStream.FlushAsync();

            return mStream.Length;
        }

        public async Task<MemoryStream> GetMemoryStream(string blobName)
        {
            await GetBlobContainer(blobName);
            CloudBlockBlob blockBlob = await GetBlob(blobName);
            MemoryStream ms = new MemoryStream();

            if (blockBlob.Exists())
            {
                // Save blob contents to a stream.
                blockBlob.DownloadToStream(ms);
            }

            return ms;
        }

        public async Task<bool> BlobExists(string blobName)
        {
            await GetBlobContainer(blobName);

            CloudBlockBlob blockBlob = await GetBlob(blobName);
            if (blockBlob.Exists())
            {
                return true;
            }
            return false;
        }

        public async Task<Uri> BlobUrl(string blobName)
        {
            await GetBlobContainer(blobName);
            string url = string.Empty;
            CloudBlockBlob blockBlob = await GetBlob(blobName);
            if (blockBlob.Exists())
            {
                return blockBlob.Uri;
            }

            return null;
        }
    }
}