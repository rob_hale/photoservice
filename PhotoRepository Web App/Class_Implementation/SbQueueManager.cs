﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using PhotoRepository_Web_App.Interfaces;
using PhotoSettings;

namespace PhotoRepository_Web_App.Classes
{
    [DataContract]
    public class ImageNames
    {
        [DataMember]
        public string ImageName { get; set; }
        [DataMember]
        public string[] ScaledImages { get; set; }
    }
    public class SbQueueManager : ISbQueueManager
    {
        public static string queueName = CloudConfigurationManager.GetSetting("ServiceBusQueueName");

        public static string connectionString = CloudConfigurationManager.GetSetting(Storage.ServiceBusConnection);
        public async Task SendImageName(string imageName)
        {
            var namespaceManager =
             NamespaceManager.CreateFromConnectionString(connectionString);

            if (!namespaceManager.QueueExists(queueName))
            {
                namespaceManager.CreateQueue(queueName);
            }

            QueueClient client = QueueClient.CreateFromConnectionString(connectionString, queueName);
            BrokeredMessage message = new BrokeredMessage("Photo Received for Scaling");
            message.ContentType = "text/plain";
            message.Properties["FileName"] = imageName;
            await client.SendAsync(message);
        }

    }
}