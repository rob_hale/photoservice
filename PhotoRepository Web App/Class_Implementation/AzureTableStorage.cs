﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using PhotoRepository_Web_App.Interfaces;
using PhotoRepository_Web_App.Models;

namespace PhotoRepository_Web_App.Class_Implementation
{
    public class AzureTableStorage : IAzureTableStorage
    {
        public AzureTableStorage(string connectionString)
        {
            ConnectionString = connectionString;
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            TableClient = cloudStorageAccount.CreateCloudTableClient();
            CloudTable photoStorageTable = TableClient.GetTableReference("PhotoMetadata");
            RetryPolicy = new RetryPolicy(ErrorDetectionStrategy, RetryStrategy);
            try
            {
                RetryPolicy.ExecuteAction(() =>
                {
                    photoStorageTable.CreateIfNotExists();
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public Task<List<GalleryModel>> GetGalleryInfoFromTable(string userName, string galleryName)
        {
            throw new NotImplementedException();
        }

        public Task<GalleryModel> GetImageInfoFromTable(string userName, string galleryName, string imageName)
        {
            throw new NotImplementedException();
        }

        public string ConnectionString { get; set; }
        public CloudStorageAccount CloudStorageAccount { get; set; }
        public CloudTableClient TableClient { get; set; }

        protected FixedInterval RetryStrategy = new FixedInterval(3, TimeSpan.FromSeconds(1));
        protected StorageTransientErrorDetectionStrategy ErrorDetectionStrategy = new StorageTransientErrorDetectionStrategy();
        protected RetryPolicy RetryPolicy = null;

        public Task WriteImageInfoToTable(GalleryModel galleryModel)
        {
            throw new NotImplementedException();
        }
    }
}