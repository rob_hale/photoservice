using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.RetryPolicies;
using PhotoRepository_Web_App.Class_Implementation;
using PhotoRepository_Web_App.Interfaces;
using SharedHubNotifier;

namespace PhotoRepository_Web_App.Classes
{
    public class PhotoPhotoBlobManager : IPhotoBlobManager
    {
        private static int _retryCount = 1;
        private static BlobRequestOptions bro = new BlobRequestOptions()
        {
            SingleBlobUploadThresholdInBytes = 1024 * 1024, //1MB, the minimum
            ParallelOperationThreadCount = 1,
        };

        private Dictionary<string, Size> photoSizes = new Dictionary<string, Size>(); 

        private IBlobConnection _blobConnection;

        private IImageManager _imageManager;

        private ISharedHubNotifier _hubNotifier;
        public PhotoPhotoBlobManager(string fileName, Guid mGuid)
        {
            FileName = fileName;
            UniqueIdentifier = mGuid;
            _blobConnection = new BlobConnection();
            _imageManager = new ImageManager();
            _hubNotifier = new PhotoUpdater(CloudConfigurationManager.GetSetting(PhotoSettings.Storage.HostUri));
        }

        public PhotoPhotoBlobManager(string fileName, Guid mGuid, IBlobConnection blobConnection, IImageManager imgManager, ISharedHubNotifier sharedHubNotifier)
        {
            FileName = fileName;
            UniqueIdentifier = mGuid;
            _blobConnection = blobConnection;
            _imageManager = imgManager;
            _hubNotifier = sharedHubNotifier;
        }

        public PhotoPhotoBlobManager(string fileName):this(fileName, new Guid())
        {
        }

        public PhotoPhotoBlobManager() : this(Guid.NewGuid().ToString(), Guid.NewGuid())
        {
        }

        public string FileName { get; set; }
        public Guid UniqueIdentifier { get; set; }
        public async Task<bool> UploadFromMemoryStream(MemoryStream mStream, String resizeFileName = null)
        {
            if (mStream == null)
            {
                throw new ArgumentNullException("mStream");
            }
            if (_blobConnection == null || _blobConnection.CloudBlobClient == null)
            {
                throw new ApplicationException("Improperly initialized blob connection");
            }
            TimeSpan backOffPeriod = TimeSpan.FromSeconds(2);
            bro.RetryPolicy = new ExponentialRetry(backOffPeriod, _retryCount);

            _blobConnection.CloudBlobClient.DefaultRequestOptions = bro;
            String blobName = Path.GetFileName(FileName);
            if (!String.IsNullOrEmpty(resizeFileName))
            {
                blobName = resizeFileName;
            }

            await _blobConnection.UploadFromMemoryStream(mStream, blobName);
            if (!string.IsNullOrEmpty(resizeFileName))
            {
                var blobUri = await _blobConnection.BlobUrl(resizeFileName);
                _hubNotifier.QueueNotification(blobUri.AbsoluteUri);
            }
            return true;
        }

        public async Task<bool> UploadFromHttpPostedFile(HttpPostedFileBase file)
        {
            if (file == null)
            {
                throw new ArgumentNullException("file");
            }

            bool retCode = false;
            await _blobConnection.GetBlobContainer(Path.GetFileName(file.FileName));

            using (MemoryStream mStream = new MemoryStream())
            {
                file.InputStream.CopyTo(mStream);
                mStream.Position = 0;
                if (string.IsNullOrEmpty(FileName))
                {
                    FileName = Path.GetFileName(file.FileName);
                }
                retCode = await this.UploadFromMemoryStream(mStream);
            }

            return retCode;
        }

        public Size GetImageSize(string imageName)
        {
            if (photoSizes.ContainsKey(imageName))
            {
                return photoSizes[imageName];
            }
            
            return new Size(0,0);
        }

        public async Task<byte[]> GetPhotoFromBlob(string fileName)
        {

            // Retrieve reference to a blob named "photo1.jpg".
            if (_blobConnection != null)
            {
                if (await _blobConnection.BlobExists(fileName))
                {
                    byte[] imageBytes;
                    // Save blob contents to a file.
                    using (MemoryStream ms = await _blobConnection.GetMemoryStream(fileName))
                    {                        
                        ms.Position = 0;
                        Bitmap bm = null;
                        imageBytes = new byte[ms.Length];
                        try
                        {
                            bm = new Bitmap(ms);
                            photoSizes[fileName] = new Size(bm.Width, bm.Height);
                        }
                        catch (Exception e)
                        {
                            var badPictureException = new ArgumentException("BadImage",e);
                            throw badPictureException;
                        }
                        ms.Position = 0;
                        await ms.ReadAsync(imageBytes,0,(int) ms.Length);
                        await ms.FlushAsync();
                    }

                    return imageBytes;
                }
            }
            return null;
        }
        public async Task<string[]> ResizePhotos(MemoryStream photoStream, string fileName)
        {
            Size dimensions = GetImageSize(fileName);
            Size[] photoSizes = CalcSizes(dimensions);
            List<string> resizedPhotoNames = new List<string>();

            IEnumerable<Task<string>> resizeTasks = from s in photoSizes select ResizeIndividualPhoto(s, fileName, photoStream);
            Task<string>[] blobDocumentTasks = resizeTasks.ToArray();

            var results = await Task.WhenAll(blobDocumentTasks);

            foreach (var result in results)
            {              
                resizedPhotoNames.Add(result);
            }
            return resizedPhotoNames.ToArray();
        }

        public async Task<string> ResizeIndividualPhoto(Size sz, string filename, MemoryStream photoStream)
        {
            string resizeFileName = string.Empty;

            // Initialize the ImageFactory using the overload to preserve EXIF metadata.
            // Load, resize, set the format and quality and save an image.
            MemoryStream outStream, resizedPhotoStream;
            Uri blobUri = null;
            try
            {
                await _imageManager.CreateImage(photoStream);
                var extension = await _imageManager.ImageExtension();

                resizedPhotoStream = await _imageManager.ResizeImage(sz);
                resizeFileName = Path.GetFileNameWithoutExtension(filename) + "_" + sz.Width + "_" + sz.Height + "." + extension;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            await UploadFromMemoryStream(resizedPhotoStream, resizeFileName);

            return resizeFileName;

        }

        private Size[] CalcSizes(Size sz)
        {
            var newSizes = new List<Size>();
            newSizes.Add(new Size(sz.Width / 2, sz.Height / 2));
            newSizes.Add(new Size(sz.Width / 3, sz.Height / 3));
            newSizes.Add(new Size(sz.Width / 4, sz.Height / 4));

            return newSizes.ToArray();
        }
    }
}
