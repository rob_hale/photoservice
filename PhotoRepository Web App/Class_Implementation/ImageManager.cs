using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ImageProcessor;
using PhotoRepository_Web_App.Interfaces;

namespace PhotoRepository_Web_App.Class_Implementation
{
    class ImageManager : IImageManager
    {
        private MemoryStream _photoStream = new MemoryStream();
        private ImageFactory _imgFactory;

        public Task<MemoryStream> CreateImage(MemoryStream ms)
        {
            var photoStream = new MemoryStream();
            {
                using (var imageFactory = new ImageFactory(preserveExifData: true))
                {
                    // Load, resize, set the format and quality and save an image.

                    _imgFactory = imageFactory.Load(ms);
                    _imgFactory.Save(_photoStream);
                }
            }
            return Task.FromResult(photoStream);
        }

        public  Task<string> ImageExtension()
        {
            if (_imgFactory == null)
            {
                throw new ArgumentNullException("imagefactory");
            }

            return Task.FromResult(_imgFactory.CurrentImageFormat.DefaultExtension);
        }

        public Task<MemoryStream> ResizeImage(Size sz)
        {
            if (_photoStream == null)
            {
                throw new ArgumentNullException("PhotoStream");
            }
            var outStream = new MemoryStream();
            _imgFactory.Load(_photoStream)
                .Resize(new Size(sz.Width, sz.Height))
                .Format(_imgFactory.CurrentImageFormat)
                .Save(outStream);

            return Task.FromResult(outStream);
        }
    }
}