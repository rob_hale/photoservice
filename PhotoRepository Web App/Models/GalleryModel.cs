﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace PhotoRepository_Web_App.Models
{
    public class GalleryModel
    {
        public List<Bitmap> GalleryThumbsList { get; set; } 
        public string UserName { get; set; }
        public string GalleryName { get; set; }
        public Bitmap SelectedImage { get; set; }
    }
}