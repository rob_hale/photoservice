﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Azure;
using Microsoft.Owin;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Owin;
using PhotoRepository_Web_App.Class_Implementation;
using PhotoSettings;

[assembly: OwinStartupAttribute(typeof(PhotoRepository_Web_App.Startup))]

namespace PhotoRepository_Web_App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AppDomain.CurrentDomain.Load(typeof (SharedHubs.PhotoReadyHub).Assembly.FullName);

            var configuration = new HubConfiguration();
            configuration.EnableJSONP = true;
            //
            // Service bus backplane can only be used on a Standard account
            //

            //string servicesBusConnectionString = CloudConfigurationManager.GetSetting(Storage.ServiceBusConnection);

            //GlobalHost.DependencyResolver.UseServiceBus(servicesBusConnectionString, "photoscaler");

            app.MapSignalR(configuration);
            ConfigureAuth(app);
        }
    }
}
