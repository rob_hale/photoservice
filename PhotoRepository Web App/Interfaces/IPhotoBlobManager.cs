﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace PhotoRepository_Web_App.Interfaces
{
    public interface IPhotoBlobManager
    {
        String FileName { get; set; }
        Guid UniqueIdentifier { get; set; }
        Task<bool> UploadFromMemoryStream(MemoryStream mStream, String size);
        Task<bool> UploadFromHttpPostedFile(HttpPostedFileBase file);
        Task<byte[]> GetPhotoFromBlob(string fileName);
        Size GetImageSize(string imageName);
        Task<string[]> ResizePhotos(MemoryStream photoStream, string fileName);
        Task<string> ResizeIndividualPhoto(Size sz, string filename, MemoryStream photoStream);
    }
}
