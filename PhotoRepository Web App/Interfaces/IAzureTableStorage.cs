﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using PhotoRepository_Web_App.Models;

namespace PhotoRepository_Web_App.Interfaces
{
    interface IAzureTableStorage
    {
        string ConnectionString
        {
            get; set;
        }
        CloudStorageAccount CloudStorageAccount
        {
            get; set;
        }

        CloudTableClient TableClient
        {
            get; set;
        }

        Task WriteImageInfoToTable(GalleryModel galleryModel);

        Task<GalleryModel> GetImageInfoFromTable(string userName, string galleryName, string imageName);

        Task<List<GalleryModel>> GetGalleryInfoFromTable(string userName, string galleryName);


    }
}
