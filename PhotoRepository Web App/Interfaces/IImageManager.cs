﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoRepository_Web_App.Interfaces
{
    public interface IImageManager
    {
        Task<MemoryStream> CreateImage(MemoryStream ms);

        Task<string> ImageExtension();

        Task<MemoryStream> ResizeImage(Size sz);

    }
}
