﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace PhotoRepository_Web_App.Interfaces
{
    public interface IBlobConnection
    {
        string ConnectionString
        {
            get; set;            
        }
        CloudStorageAccount CloudStorageAccount
        {
            get; set;            
        }

        CloudBlobClient CloudBlobClient
        {
            get; set;            
        }
       
        CloudBlobContainer BlobContainer { get; set; }

        Task<CloudBlobContainer> GetBlobContainer(String blobName);

        Task<CloudBlockBlob> GetBlob(string blobName);

        Task<long> UploadFromMemoryStream(MemoryStream ms, string blobName);

        Task<MemoryStream> GetMemoryStream(string blobName);

        Task<bool> BlobExists(string blobName);

        Task<Uri> BlobUrl(string blobName);
    }
}
