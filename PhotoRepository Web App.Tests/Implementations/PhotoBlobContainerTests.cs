﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NSubstitute;
using NUnit.Framework;
using PhotoRepository_Web_App.Classes;
using PhotoRepository_Web_App.Controllers;
using PhotoRepository_Web_App.Class_Implementation;
using PhotoRepository_Web_App.Interfaces;

namespace PhotoRepository_Web_App.Tests.Implementations
{
    [TestFixture]
    public class PhotoBlobContainerTests
    {
        [Test]
        public void Constructor()
        {
            var photoBlobConnection = new BlobConnection();
            photoBlobConnection.Should().NotBeNull();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void ConstructorBadString()
        {
            var photoBlobConnection = new BlobConnection("blargy");
        }


        [Test]
        public async void PhotoBlobContainer_test()
        {

            //var pBlobConnection = new BlobConnection("UseDevelopmentStorage=true");

            var pBlobConnection = Substitute.For<IBlobConnection>();
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("UseDevelopmentStorage=true");
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            pBlobConnection.BlobContainer = (container);
            pBlobConnection.CloudBlobClient.Returns(blobClient);
            await pBlobConnection.GetBlobContainer("testImages");
            pBlobConnection.BlobContainer.Should().NotBeNull();
        }


    }
}
