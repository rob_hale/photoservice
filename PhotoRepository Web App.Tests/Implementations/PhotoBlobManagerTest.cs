﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Blob.Protocol;
using NSubstitute;
using NUnit.Framework;
using PhotoRepository_Web_App.Classes;
using PhotoRepository_Web_App.Class_Implementation;
using PhotoRepository_Web_App.Interfaces;
using SharedHubNotifier;

namespace PhotoRepository_Web_App.Tests.Implementations
{
    [TestFixture]

    public class PhotoBlobManagerTest
    {
        [Test]
        [NUnit.Framework.ExpectedException(typeof(ArgumentNullException))]
        public async void UploadFromMemoryStream_memory_stream_null()
        {
            var pBlobConnection = Substitute.For<IBlobConnection>();
            var pImgManager = Substitute.For<IImageManager>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();
            var photoBlobManager = new PhotoPhotoBlobManager("testFile.img", Guid.NewGuid(), pBlobConnection, pImgManager,pSignalRManager);
            await photoBlobManager.UploadFromMemoryStream(null, "nofile");
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public async void UploadFromMemoryStream_blob_connection_null()
        {
            byte[] testData = Guid.NewGuid().ToByteArray();
            var pImgManager = Substitute.For<IImageManager>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();


            var ms = new MemoryStream(testData);
            var photoBlobManager = new PhotoPhotoBlobManager("testFile.img", Guid.NewGuid(), null, pImgManager,pSignalRManager);
            await photoBlobManager.UploadFromMemoryStream(ms, "nofile");
        }
        [Test]
        public async void UploadFromMemoryStream_success()
        {
            var pBlobConnection = Substitute.For<IBlobConnection>();
            var pImgManager = Substitute.For<IImageManager>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();

            byte[] testData = Guid.NewGuid().ToByteArray();
            var ms = new MemoryStream(testData);
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("UseDevelopmentStorage=true");
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            pBlobConnection.BlobContainer=(container);
            pBlobConnection.CloudBlobClient.Returns(blobClient);
            pBlobConnection.UploadFromMemoryStream(ms, Arg.Any<string>()).Returns(Task.FromResult(ms.Length));
            pBlobConnection.BlobUrl(Arg.Any<string>()).Returns(Task.FromResult(new Uri("http://www.contoso.com/fakefile")));

            var photoBlobManager = new PhotoPhotoBlobManager("testFile.img", Guid.NewGuid(), pBlobConnection,pImgManager,pSignalRManager);

            await photoBlobManager.UploadFromMemoryStream(ms, "testFile.img");
        }

        [Test]
        public async void GetPhotoFromBlob_success()
        {
            var pBlobConnection = Substitute.For<IBlobConnection>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();

            FileStream fs = new FileStream("./loud_howard_by_tominabox1.png", FileMode.Open, FileAccess.Read);
            MemoryStream ms = new MemoryStream();
            fs.CopyTo(ms);
            pBlobConnection.BlobExists("./loud_howard_by_tominabox1.png").Returns(Task.FromResult(true));
            pBlobConnection.GetMemoryStream("./loud_howard_by_tominabox1.png").Returns(Task.FromResult(ms));
            var pImgManager = Substitute.For<IImageManager>();

            var photoBlobManager = new PhotoPhotoBlobManager("./loud_howard_by_tominabox1.png", Guid.NewGuid(), pBlobConnection,pImgManager, pSignalRManager);
            byte[] imageBytes = await photoBlobManager.GetPhotoFromBlob("./loud_howard_by_tominabox1.png");
            pBlobConnection.Received().BlobExists("./loud_howard_by_tominabox1.png");
            pBlobConnection.Received().GetMemoryStream("./loud_howard_by_tominabox1.png");
            imageBytes.Length.Should().BeGreaterThan(0);
            imageBytes.Length.Should().Be((int) fs.Length);
        }

        [Test]
        public async void GetPhotoSizes_success()
        {
            var pBlobConnection = Substitute.For<IBlobConnection>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();

            FileStream fs = new FileStream("./loud_howard_by_tominabox1.png", FileMode.Open, FileAccess.Read);
            MemoryStream ms = new MemoryStream();
            fs.CopyTo(ms);
            pBlobConnection.BlobExists("loud_howard_by_tominabox1.png").Returns(Task.FromResult(true));
            pBlobConnection.GetMemoryStream("loud_howard_by_tominabox1.png").Returns(Task.FromResult(ms));
            var pImgManager = Substitute.For<IImageManager>();

            var photoBlobManager = new PhotoPhotoBlobManager("loud_howard_by_tominabox1.png", Guid.NewGuid(), pBlobConnection, pImgManager,pSignalRManager);
            byte[] imageBytes = await photoBlobManager.GetPhotoFromBlob("loud_howard_by_tominabox1.png");

            Size sz1 = photoBlobManager.GetImageSize("loud_howard_by_tominabox1.png");
            sz1.Width.Should().NotBe(0);
            sz1.Height.Should().NotBe(0);

            Size sz2 = photoBlobManager.GetImageSize("notexists");
            sz2.Width.Should().Be(0);
            sz2.Height.Should().Be(0);
        }
        [Test]
        public async void GetPhotoFromBlob_null_connection()
        {
            var photoBlobManager = new PhotoPhotoBlobManager("./loud_howard_by_tominabox1.png", Guid.NewGuid(), null, null, null);
            byte[] imageBytes = await photoBlobManager.GetPhotoFromBlob("./loud_howard_by_tominabox1.png");
            imageBytes.Should().BeNull();
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public async void GetPhotoFromBlob_bad_image_data()
        {
           var pBlobConnection = Substitute.For<IBlobConnection>();
            var pImageManager = Substitute.For<IImageManager>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();

            MemoryStream ms = new MemoryStream();
            pBlobConnection.BlobExists("./loud_howard_by_tominabox1.png").Returns(Task.FromResult(true));
            pBlobConnection.GetMemoryStream("./loud_howard_by_tominabox1.png").Returns(Task.FromResult(ms));

            var photoBlobManager = new PhotoPhotoBlobManager("./loud_howard_by_tominabox1.png", Guid.NewGuid(), pBlobConnection, pImageManager,pSignalRManager);
            byte[] imageBytes = await photoBlobManager.GetPhotoFromBlob("./loud_howard_by_tominabox1.png");

        }

        [Test]
        public async void ResizeIndividualPhoto_success()
        {
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();
            var pBlobConnection = Substitute.For<IBlobConnection>();
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("UseDevelopmentStorage=true");
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            pBlobConnection.BlobContainer = (container);
            pBlobConnection.CloudBlobClient.Returns(blobClient);
            pBlobConnection.BlobUrl(Arg.Any<string>())
                .Returns(Task.FromResult(new Uri("http://www.contoso.com/fakeDirectory")));
            var pImageManager = Substitute.For<IImageManager>();
            FileStream fs = new FileStream("./loud_howard_by_tominabox1.png", FileMode.Open, FileAccess.Read);
            MemoryStream ms = new MemoryStream();
            fs.CopyTo(ms);
            pImageManager.CreateImage(ms).Returns(Task.FromResult(ms));
            pImageManager.ImageExtension().Returns(Task.FromResult("png"));
            pImageManager.ResizeImage(Arg.Any<Size>()).Returns(Task.FromResult(ms));
            pBlobConnection.UploadFromMemoryStream(ms, Arg.Any<string>()).Returns(Task.FromResult(ms.Length));
            var photoBlobManager = new PhotoPhotoBlobManager("./loud_howard_by_tominabox1.png", Guid.NewGuid(), pBlobConnection, pImageManager,pSignalRManager);

            string name = await photoBlobManager.ResizeIndividualPhoto(new Size(320, 640), "./loud_howard_by_tominabox1.png", ms);
            name.Should().NotBeNull();
            name.Should().Contain("320");
            name.Should().Contain("640");
            pImageManager.Received().CreateImage(ms);
            pImageManager.Received().ImageExtension();
            pImageManager.Received().ResizeImage(Arg.Any<Size>());
            pBlobConnection.Received().UploadFromMemoryStream(ms, Arg.Any<string>());
        }

        [Test]
        public async void ResizePhotos_success()
        {
            var pBlobConnection = Substitute.For<IBlobConnection>();
            var pSignalRManager = Substitute.For<ISharedHubNotifier>();

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("UseDevelopmentStorage=true");
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            pBlobConnection.BlobContainer = (container);
            pBlobConnection.CloudBlobClient.Returns(blobClient);
            pBlobConnection.BlobUrl(Arg.Any<String>())
                .Returns(Task.FromResult(new Uri("http://www.contoso.com/fakeDirtectory")));
            var pImageManager = Substitute.For<IImageManager>();
            FileStream fs = new FileStream("./loud_howard_by_tominabox1.png", FileMode.Open, FileAccess.Read);
            MemoryStream ms = new MemoryStream();
            fs.CopyTo(ms);
            pImageManager.CreateImage(ms).Returns(Task.FromResult(ms));
            pImageManager.ImageExtension().Returns(Task.FromResult("png"));
            pImageManager.ResizeImage(Arg.Any<Size>()).Returns(Task.FromResult(ms));
            pBlobConnection.UploadFromMemoryStream(ms, Arg.Any<string>()).Returns(Task.FromResult(ms.Length));

            var photoBlobManager = new PhotoPhotoBlobManager("./loud_howard_by_tominabox1.png", Guid.NewGuid(), pBlobConnection, pImageManager,pSignalRManager);
            await photoBlobManager.ResizePhotos(ms, "./loud_howard_by_tominabox1.png");

        }
    }
}
