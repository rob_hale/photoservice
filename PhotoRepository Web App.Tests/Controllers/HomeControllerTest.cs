﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FluentAssertions;
using FluentAssertions.Mvc;
using NSubstitute;
using NUnit.Framework;
using PhotoRepository_Web_App;
using PhotoRepository_Web_App.Controllers;
using PhotoRepository_Web_App.Interfaces;

namespace PhotoRepository_Web_App.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        [Test]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            result.Should().NotBeNull();

        }

        [Test]
        public async void IndexPostTestFileUpload()
        {
            var blobUploader = Substitute.For<IPhotoBlobManager>();
            var sbQueueManager = Substitute.For<ISbQueueManager>();
            var file = Substitute.For<HttpPostedFileBase>();
            file.ContentLength.Returns(100);
            file.FileName.Returns("blobImage");
            blobUploader.UploadFromHttpPostedFile(file).Returns(Task.FromResult(true));
            sbQueueManager.SendImageName(file.FileName).Returns(Task.FromResult(true));

            HomeController controller = new HomeController(blobUploader, sbQueueManager);
            ViewResult result = await controller.Index(file) as ViewResult;

            result.Should().NotBeNull();
            result.Model.Should().Be(file.ContentLength);
            blobUploader.Received().UploadFromHttpPostedFile(file);
            sbQueueManager.Received().SendImageName(file.FileName);
        }
        [Test]
        public async void IndexPostTestFileUploadNullFile()
        {
            var blobUploader = Substitute.For<IPhotoBlobManager>();
            var sbQueueManager = Substitute.For<ISbQueueManager>();

            var file = Substitute.For<HttpPostedFileBase>();
            blobUploader.UploadFromHttpPostedFile(Arg.Any<HttpPostedFileBase>()).Returns(x => { throw new Exception();});
            sbQueueManager.SendImageName(Arg.Any<String>()).Returns(x => { throw new Exception(); });

            HomeController controller = new HomeController(blobUploader,sbQueueManager);
            ViewResult result = await controller.Index(null) as ViewResult;
            result.Should().NotBeNull();
            result.Model.Should().Be(0);
            blobUploader.Received(0).UploadFromHttpPostedFile(file);
            sbQueueManager.Received(0).SendImageName(file.FileName);

        }

    }
}
