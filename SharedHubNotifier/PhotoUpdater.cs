﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Azure;
using PhotoSettings;

namespace SharedHubNotifier
{
    public class PhotoUpdater:ISharedHubNotifier
    {
        private IHubProxy _photoHubProxy;
        private Subject<string> _completedImages; 

        public PhotoUpdater(string currentUrl)
        {
            var hubConnection = new HubConnection(currentUrl);
            _photoHubProxy = hubConnection.CreateHubProxy("photoReadyHub");
            InitMessages();
            hubConnection.Start().Wait();
        }

        private void InitMessages()
        {
            _completedImages = new Subject<string>();
            _completedImages.Delay(TimeSpan.FromMilliseconds(500))
                .Subscribe(imageData =>  SendSharedPhotoName(imageData) );
        }

        public async Task SendSharedPhotoName(String photoName)
        {
            if (photoName == null) return;
            Console.WriteLine("Sending SignalR message for photo {0} at {1}",photoName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            await _photoHubProxy.Invoke("BroadcastMessage", photoName);
        }

        public void QueueNotification(string photoName)
        {
            Console.WriteLine("Queuing the photo {0} for SignalR ", photoName );
            _completedImages.OnNext(photoName);
        }
    }
}
