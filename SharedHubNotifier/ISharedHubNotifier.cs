﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedHubNotifier
{
    public interface ISharedHubNotifier
    {
        Task SendSharedPhotoName(string photoName);

        void QueueNotification(string s);
    }
}
